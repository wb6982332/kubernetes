provider "aws" {
  region = "us-east-2"
}

variable "myhosts" {
  type = map
  default = {
    kmaster = "t2.medium"
    kworker1 = "t2.micro"
    kworker2 = "t2.micro"
  }
}

variable "keyfile" {
  default = "terraform"
}

module "server" {
  for_each = var.myhosts
  type = each.value
  servername = each.key
  keyfile = var.keyfile
  source = "./modules/instance"  
  key = "ubuntu"
  volumesize = "8"
}
