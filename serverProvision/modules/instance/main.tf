resource "aws_instance" "ex1" {
  ami = var.ami[var.key]
  instance_type = var.type
  key_name = var.keyfile

  root_block_device {
    volume_size = var.volumesize
  }

  tags = {
    Name = var.servername
  }
}





