variable "ami" {
  type = map
  default = {
    "ubuntu" = "ami-06c4532923d4ba1ec"
    "amazon" = "ami-092b51d9008adea15"
    "redhat" = "ami-02b8534ff4b424939"    
  }
}

variable "key" {
  default = "ubuntu"
}

variable "type" {
  default = "t2.micro"
}

variable "keyfile" {
  default = "terraform"
}

variable "servername" {
  default = "servername"
}

variable "volumesize" {
  type = number
  default = "8"  
}